import javax.swing.JMenu;
import javax.swing.JMenuBar;

/**
 * 
 */

/**
 * @author Vincent Sch�nzer
 *
 */
public class MenuLeiste extends JMenuBar {
	
	  private JMenu spiel, anzeige, hilfe;
	/**
	 * @param spielfenster
	 */
	public MenuLeiste(Spielfenster spielfenster) {
		this.spiel = new JMenu("Spiel");
	    this.anzeige = new JMenu("Anzeige");
	    this.hilfe = new JMenu("Hilfe");
	    
	    add(this.spiel);
	    add(this.anzeige);
	    add(this.hilfe);
	}

}
