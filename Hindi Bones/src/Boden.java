public class Boden extends Spielelement {
	
	private boolean accessible = true;
	
	public Boden(Spielfeld spielfeld) {
		super(spielfeld, "img/boden.png");
	}
	
	public boolean accessible() {
		return this.accessible;
	}

}
