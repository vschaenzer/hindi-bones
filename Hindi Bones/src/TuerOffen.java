public class TuerOffen extends Spielelement {
	
	private boolean accessible = true;
	
	public TuerOffen(Spielfeld spielfeld) {
		super(spielfeld, "img/tueroffen.png");
	}

	@Override
	public boolean accessible() {
		return this.accessible;
	}
	
	

}
