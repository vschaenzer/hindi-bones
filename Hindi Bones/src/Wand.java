public class Wand extends Spielelement {
	
	private boolean accessible = false;
	
	public Wand(Spielfeld spielfeld) {
		super(spielfeld, "img/wand.png");
	}
	
	public boolean accessible() {
		return this.accessible;
	}
}
