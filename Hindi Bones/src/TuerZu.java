public class TuerZu extends Spielelement {
	
	private boolean accessible = false;
	
	public TuerZu(Spielfeld spielfeld) {
		super(spielfeld, "img/tuer.png");
	}

	@Override
	public boolean accessible() {
		return this.accessible;
	}
	
	

}
