import java.awt.Image;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;


public abstract class Spielelement {

	private Image Grafik;
	protected Spielfeld spielfeld;
	
	
	
	public Spielelement(Spielfeld spielfeld, String Grafik_URL){
		this.spielfeld = spielfeld;

			try {
				Grafik = ImageIO.read(new File(Grafik_URL));
			}
		    catch (IOException e)
		    {
		      System.err.println("Ein Bild konnte nicht geladen werden.");
		    }
			
		}

	public abstract boolean accessible();
	
	public Image getBild(){
		return Grafik;
	}
	
}
