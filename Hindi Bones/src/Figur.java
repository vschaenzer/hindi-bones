import java.util.Iterator;





public abstract class Figur extends Spielelement {
	
	protected int xPos;
	protected int yPos;
	
	private int health = 100;
	private int strange = 10;
	
	
	public Figur(Spielfeld spielfeld, String path, int xPos, int yPos) {
		super(spielfeld, path);
		setPos(xPos, yPos);
	}
	
	
	public void setPos(int xPos, int yPos) {
		this.xPos = xPos;
		this.yPos = yPos;
	}
	
	
	public void GetAttack (int strange) {
		System.out.println("Auha!!!");
		this.health -= strange;
		
		if (this.health <= 0) {
			spielfeld.removeSpielelement(xPos,yPos,2);
		}
	}
	
	public void Attack (Spielfeld spielfeld){
		final Iterator<Figur> cf_iterator = spielfeld.GetCloseFigures(this).iterator();
		while(cf_iterator.hasNext()){
			cf_iterator.next().GetAttack(this.strange);
		}
	}
	
	public int get_xPos() {
		return this.xPos;
	}
	
	public int get_yPos() {
		return this.yPos;
	}
	
	
	
	
	
	
	
	
	
	
	
	public abstract void move(Spielfeld spielfeld, char direction);
	
	
	private int energie;
	
	public void setEnergie(int energie) {
		this.energie = energie;
	}
	public int getEnergie() {
		return energie;
	}
	
}
