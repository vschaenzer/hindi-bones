import javax.swing.*;
import java.awt.*;

public class Statusleiste extends JPanel {

	private static final long serialVersionUID = 1L;
	
	private final int BREITE = 400;
	private final int HOEHE = 15;
	
	public Statusleiste() {
		this.setPreferredSize(new Dimension(BREITE, HOEHE));
		this.setBackground(Color.BLACK);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.WHITE);
		g.drawString("Spielername", 2, HOEHE - 3);
		g.drawString("Leben: 3", 330, HOEHE - 3);
	}
	
}
