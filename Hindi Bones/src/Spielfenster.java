import javax.swing.*;

import java.awt.*;

public class Spielfenster extends JFrame {

	private static final long serialVersionUID = 1L;
	
	private final int BREITE = 400;
	private final int HOEHE = 415;
	
	public Spielfenster() {
		this.setLocation(500, 150);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(BREITE, HOEHE);
		this.setTitle("Hindi Bones - Lost in Labyrinth");
		
		this.setLayout(new BorderLayout(1,1));
		
		//MenuLeiste menuleiste = new MenuLeiste(this);
		//this.add(menuleiste, "North");
		
		
		Statusleiste sl = new Statusleiste();
		this.add(sl, BorderLayout.SOUTH);
		
		
		
		Spielfeld sf = new Spielfeld();
		this.addKeyListener(sf);
		this.add(sf, BorderLayout.CENTER);
		
		this.setResizable(false);
		this.setVisible(true);
	}
	
}
