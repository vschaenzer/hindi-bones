import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.LinkedList;

import javax.swing.*;

public class Spielfeld extends JPanel implements KeyListener {

	private static final long serialVersionUID = 1L;
	
	private final int BREITE = 400;
	private final int HOEHE = 400;

	
	private Spieler SPIELER;
	
	Spielelement[][][] Feld = new Spielelement[16][16][4];
	//Ebene 0 : Boden oder Wand oder Tuer
	//Ebene 1 : Iteams
	//Ebene 2 : Monsta
	//Ebene 3 : Spieler
	
	
	public Spielfeld() {
		try {
			BufferedReader  in = new BufferedReader (new FileReader("lvl/level0.txt"));
				
			for (int j = 0; j < 16; j++) {
				String line = in.readLine();
				for (int i = 0; i < 16; i++) { 
					System.out.print(line.charAt(i));
					switch (line.charAt(i)) {
					
						case '0': Feld[j][i][0] = new Wand(this) ; break;
						case '1': Feld[j][i][0] = new Boden(this) ; break;
						case '3': Feld[j][i][0] = new TuerZu(this) ; break;
						case '4': {
							Feld[j][i][0] = new TuerOffen(this);
							Feld[j][i][3] = SPIELER = new Spieler(this,j,i);
							break;
						}
						
						case '2': {
							Feld[j][i][0] = new Boden(this);
							Feld[j][i][1] = new Schluessel(this);
							break;
						}
						
						case '5': {
							Feld[j][i][0] = new Boden(this);
							Feld[j][i][2] = new Monster(this,j,i);
							break;
						}
					}
				}
				System.out.println();
			}
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(1);
		}
		
		this.setPreferredSize(new Dimension(BREITE, HOEHE));
		this.setBackground(Color.DARK_GRAY);
	}
	
	public LinkedList<Figur> GetCloseFigures (Figur Me) {
		LinkedList<Figur> close_figures = new LinkedList<Figur>();
		
		
		for (int j = -1; j <= 1; j++) {
			for (int i = -1; i <= 1; i++) { 
				if (Feld[Me.get_xPos()+j][Me.get_yPos()+i][2] != null) close_figures.add((Figur) Feld[Me.get_xPos()+j][Me.get_yPos()+i][2]);
			}
		}
		
		return close_figures;
	}
	
	public void removeSpielelement(int j, int i, int typ) {
		this.Feld[j][i][typ] = null;
	}
	
	
	public void paint(Graphics g) {
		
		for (int j = 0; j < Feld.length; j++) {
			for (int i = 0; i < Feld[j].length; i++) {
				for (int k = 0; k < Feld[j][i].length; k++) {
					if (Feld[j][i][k] != null) g.drawImage(Feld[j][i][k].getBild(), i * 25, j * 25, 25, 25, null);
				}
			}
		}
		repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_UP) SPIELER.move(this, 'N');
		if (e.getKeyCode() == KeyEvent.VK_DOWN) SPIELER.move(this, 'S');
		if (e.getKeyCode() == KeyEvent.VK_LEFT) SPIELER.move(this, 'W');
		if (e.getKeyCode() == KeyEvent.VK_RIGHT) SPIELER.move(this, 'E');
		if (e.getKeyCode() == KeyEvent.VK_Q) SPIELER.Attack(this);
		
		repaint();
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
	}

	@Override
	public void keyTyped(KeyEvent arg0) {
	}

}
