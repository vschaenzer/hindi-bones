public class Spieler extends Figur {

	public Spieler(Spielfeld spielfeld, int xPos, int yPos) {
		super(spielfeld, "img/spieler.png", xPos, yPos);
	}
	
	
	@Override
	public void move(Spielfeld spielfeld, char direction) {
		
		int xPos_new = this.xPos;
		int yPos_new = this.yPos;
		
		switch (direction) {
			case 'N': xPos_new--; break;
			case 'E': yPos_new++; break;
			case 'S': xPos_new++; break;
			case 'W': yPos_new--; break;
		}
		
		if (spielfeld.Feld[xPos_new][yPos_new][0].accessible()) {
			spielfeld.Feld[this.xPos][this.yPos][3] = null;
			spielfeld.Feld[xPos_new][yPos_new][3] = this;
			this.xPos = xPos_new;
			this.yPos =yPos_new;
		}
		
	}



	@Override
	public boolean accessible() {
		// TODO Auto-generated method stub
		return false;
	}

	
}
